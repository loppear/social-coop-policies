# Social.coop Code of Conduct

DRAFT

## Purpose

A primary goal of Social.coop is to be inclusive to the largest number of contributors, with the most varied and diverse backgrounds possible. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion (or lack thereof).

This code of conduct outlines our expectations for all those who participate in our community, as well as the consequences for unacceptable behavior.

## Principles

We invite all those who participate in Social.coop to help us create safe and positive experiences for everyone.

We incorporate the [Internation Co-operative Alliance](https://ica.coop/en/whats-co-op/co-operative-identity-values-principles) values and principles as a framework for constructively and collaboratively encouraging these experiences. Co-operatives are based on the values of self-help, self-responsibility, democracy, equality, equity and solidarity. In the tradition of their founders, co-operative members believe in the ethical values of honesty, openness, social responsibility and caring for others.

1. Voluntary and Open Membership
2. Democratic Member Control
3. Member Economic Participation
4. Autonomy and Independence
5. Education, Training and Information
6. Co-operation among Co-operatives
7. Concern for Community

All members of Social.coop are expected to treat others with respect and behave in accordance with these principles and code of conduct, with any policies governing appropriate behavior defined by their employers, and with any related laws applicable in their geographic location.

## Unacceptable Behavior

Harassment will not be tolerated in any form, including but not limited to harassment based on gender, gender identity and expression, gender presentation, sexual orientation, disability, mental illness, neuro(a)typicality, physical appearance, body size, race, age, color, political or religious affiliation, national origin or identity, technical or other ability, pregnancy, socioeconomic status, a person’s lifestyle choices and practices (including those relating to food, health, parenting, drugs and employment) or any other status protected by laws in which interaction takes place.

Harassment includes the use of abusive, offensive or degrading language, intimidation, stalking, harassing photography or recording, inappropriate physical contact, sexual imagery and unwelcome sexual advances or requests for sexual favors, and deliberate “outing” of any aspect of a person’s identity without their consent except as necessary to protect others from intentional abuse.

## Consequences of Unacceptable Behavior

Unacceptable behavior from any community member, including sponsors and those with decision-making or administrative authority, will not be tolerated.

Anyone asked to stop unacceptable behavior is expected to comply immediately. Members engaging in harassing behavior may be asked to participate in a process to address this behavior, or face restrictions including temporary and permanent banning from the Social.coop community.

## Reporting Guidelines

If you are being harassed, notice that someone else is being harassed, or have any other concerns relating to harassment or unacceptable behavior, please report it by emailing conduct@social.coop. Please include in your report your name and contact details, names or usernames of any individuals involved, names of additional witnesses, your account of what occurred and if you believe the incident is ongoing, and any links to other available records of the incident. All reports will be handled with discretion. If you are concerned that sending to the conduct@social.coop alias may end up going to an involved party, please contact one (or preferably two) of the [WHAT IS A GOOD FALLBACK?] to determine how best to address your report.


## License and attribution

This Code of Conduct is distributed under a Creative Commons Attribution-ShareAlike license.

Portions of text dervied from the International Cooperative Alliance Principles, Citizen Code of Conduct, Digital Life Collective Code of Conduct, Django Code of Conduct and the Geek Feminism Anti-Harassment Policy.

